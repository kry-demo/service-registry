package com.example.kry.service;


import com.example.kry.model.ServiceInstance;
import com.example.kry.repository.ServiceInstanceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;

@ExtendWith(MockitoExtension.class)
class HealthCheckServiceTest {

    @InjectMocks
    private HealthCheckService healthCheckService;

    @Mock
    private ServiceInstanceRepository serviceInstanceRepository;

    @Mock
    private ServiceRegistryUpdateService serviceRegistryUpdateService;

    private ServiceInstance serviceInstanceFromDb;

    @BeforeEach
    void setUp() {
        serviceInstanceFromDb = new ServiceInstance();
        serviceInstanceFromDb.setId(1L);
        serviceInstanceFromDb.setName("TestServiceInstanceFromDB");
        serviceInstanceFromDb.setEndpoint("http://testurl.com/health");
        serviceInstanceFromDb.setDateCreated(LocalDateTime.of(2001,11,30,1,25));
    }

    @Test
    public void testRunHealthCheck() {

        Mockito.when(serviceInstanceRepository.findAll())
                .thenReturn(Arrays.asList(serviceInstanceFromDb));
        healthCheckService.runHealthCheck();
        Mockito.verify(serviceRegistryUpdateService, Mockito.times(1))
                .fetchAndUpdateServiceStatus(serviceInstanceFromDb);

    }
}