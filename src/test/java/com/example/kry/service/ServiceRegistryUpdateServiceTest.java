package com.example.kry.service;

import com.example.kry.dto.ServiceInstanceHealthDto;
import com.example.kry.model.HealthRecord;
import com.example.kry.model.ServiceInstance;
import com.example.kry.util.enumeration.ServerHealthStatus;
import com.example.kry.util.enumeration.ServiceStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class ServiceRegistryUpdateServiceTest {

    @InjectMocks
    private ServiceRegistryUpdateService serviceRegistryUpdateService;

    @Mock
    private HealthRecordService healthRecordService;

    @Mock
    private ServerStatusPollerService serverStatusPollerService;

    @Mock
    SimpMessagingTemplate simpMessagingTemplate;

    @Captor ArgumentCaptor<ServiceInstanceHealthDto> captor;

    private ServiceInstance serviceInstanceFromDb;

    @BeforeEach
    void setUp() {
        serviceInstanceFromDb = new ServiceInstance();
        serviceInstanceFromDb.setId(1L);
        serviceInstanceFromDb.setName("TestServiceInstanceFromDB");
        serviceInstanceFromDb.setEndpoint("http://testurl.com/health");
        serviceInstanceFromDb.setDateCreated(LocalDateTime.of(2001,11,30,1,25));
    }

    @Test
    public void testWhenStatusIsOkThenReturnSuccessHealthRecord() {

        Mockito.when(serverStatusPollerService.pollServerHealthStatus(any()))
                .thenReturn(ServerHealthStatus.SUCCESS);

        serviceRegistryUpdateService.fetchAndUpdateServiceStatus(serviceInstanceFromDb);

        Mockito.verify(healthRecordService, Mockito.times(1))
                .save(any(HealthRecord.class));
        Mockito.verify(simpMessagingTemplate)
                .convertAndSend(Mockito.eq("/status/instance-health"), captor.capture());

        ServiceInstanceHealthDto serviceInstanceHealthDto = captor.getValue();
        assertThat(serviceInstanceHealthDto.getInstanceId()).isEqualTo(1L);
        assertThat(serviceInstanceHealthDto.getServiceStatus()).isEqualTo(ServiceStatus.OK);
    }

    @Test
    public void testWhenStatusIsFailThenReturnFailedHealthRecord() {

        Mockito.when(serverStatusPollerService.pollServerHealthStatus(any()))
                .thenReturn(ServerHealthStatus.FAILED);

        serviceRegistryUpdateService.fetchAndUpdateServiceStatus(serviceInstanceFromDb);

        Mockito.verify(healthRecordService, Mockito.times(1))
                .save(any(HealthRecord.class));
        Mockito.verify(simpMessagingTemplate)
                .convertAndSend(Mockito.eq("/status/instance-health"), captor.capture());

        ServiceInstanceHealthDto serviceInstanceHealthDto = captor.getValue();
        assertThat(serviceInstanceHealthDto.getInstanceId()).isEqualTo(1L);
        assertThat(serviceInstanceHealthDto.getServiceStatus()).isEqualTo(ServiceStatus.FAIL);
    }
}