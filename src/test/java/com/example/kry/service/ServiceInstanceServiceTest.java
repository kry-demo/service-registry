package com.example.kry.service;

import com.example.kry.model.ServiceInstance;
import com.example.kry.repository.ServiceInstanceRepository;
import com.example.kry.util.enumeration.ServiceStatus;
import com.example.kry.util.exception.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ServiceInstanceServiceTest {

    @Mock
    ServiceInstanceRepository serviceInstanceRepository;

    @InjectMocks
    ServiceInstanceService serviceInstanceService;

    private ServiceInstance serviceInstanceFromDb;

    @BeforeEach
    void setUp() {

        serviceInstanceFromDb = new ServiceInstance();
        serviceInstanceFromDb.setId(1L);
        serviceInstanceFromDb.setName("TestServiceInstanceFromDB");
        serviceInstanceFromDb.setEndpoint("http://testurl.com/health");
        serviceInstanceFromDb.setDateCreated(LocalDateTime.of(2001,11,30,1,25));

    }

    @Test
    public void testWhenServiceSavedThenShouldSuccess() {
        Mockito.when(serviceInstanceRepository.save(Mockito.any(ServiceInstance.class)))
                .then( invocation -> {
                    ServiceInstance serviceInstance = invocation.getArgument(0);
                    assertThat(serviceInstance.getId()).isNull();
                    assertThat(serviceInstance.getDateCreated()).isNotNull();
                    return serviceInstanceFromDb;
                });
        Mockito.when(serviceInstanceRepository.findByName("TestServiceInstance"))
                .thenReturn(Optional.empty());

        try {
            ServiceInstance serviceInstance = new ServiceInstance();
            serviceInstance.setName("TestServiceInstance");
            serviceInstance.setEndpoint("http://testurl.com/health");

            ServiceInstance serviceInstanceSaved = serviceInstanceService.save(serviceInstance);

            assertThat(serviceInstanceSaved).isNotNull();
            assertThat(serviceInstanceSaved.getId()).isEqualTo(1L);
            assertThat(serviceInstanceSaved.getName()).isEqualTo("TestServiceInstanceFromDB");
            assertThat(serviceInstanceSaved.getEndpoint()).isEqualTo("http://testurl.com/health");
            assertThat(serviceInstanceSaved.getDateCreated()).isEqualTo(LocalDateTime.of(2001,11,30,1,25));

        } catch (BadRequestException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWhenGetAllThenShouldReturnAll() {
        Mockito.when(serviceInstanceRepository.findAll())
                .thenReturn(Arrays.asList(serviceInstanceFromDb));

        List<ServiceInstance> serviceInstancesResult = serviceInstanceService.getAll();
        assertThat(serviceInstancesResult).isNotNull();
        assertThat(serviceInstancesResult).hasSize(1);

        ServiceInstance serviceInstanceResult = serviceInstancesResult.get(0);
        assertThat(serviceInstanceResult).isNotNull();
        assertThat(serviceInstanceResult.getId()).isEqualTo(1L);
        assertThat(serviceInstanceResult.getName()).isEqualTo("TestServiceInstanceFromDB");
        assertThat(serviceInstanceResult.getEndpoint()).isEqualTo("http://testurl.com/health");
    }
}