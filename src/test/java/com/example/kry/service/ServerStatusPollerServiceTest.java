package com.example.kry.service;

import com.example.kry.util.enumeration.ServerHealthStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ServerStatusPollerServiceTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    ServerStatusPollerService serverStatusPollerService;

    @Test
    public void testWhenEndpintIsOkThenReturnSuccess() {

        ResponseEntity<String> restResponse = new ResponseEntity(HttpStatus.OK);
        Mockito.when(restTemplate.getForEntity("invalidUrl", String.class))
                        .thenReturn(restResponse);
        ServerHealthStatus serverHealthStatus = serverStatusPollerService.pollServerHealthStatus("invalidUrl");
        assertThat(serverHealthStatus).isEqualTo(ServerHealthStatus.SUCCESS);
    }

    @Test
    public void testWhenEndpintThrowServerErrorThenReturnFailed() {

        ResponseEntity<String> restResponse = new ResponseEntity(HttpStatus.FORBIDDEN);
        Mockito.when(restTemplate.getForEntity("validUrl", String.class))
                .thenReturn(restResponse);
        ServerHealthStatus serverHealthStatus = serverStatusPollerService.pollServerHealthStatus("validUrl");
        assertThat(serverHealthStatus).isEqualTo(ServerHealthStatus.FAILED);
    }
}