package com.example.kry.controller;

import com.example.kry.dto.ServiceInstanceDto;
import com.example.kry.mappers.ServiceRegistryMapper;
import com.example.kry.model.ServiceInstance;
import com.example.kry.service.ServiceInstanceService;
import com.example.kry.util.enumeration.ServiceStatus;
import com.example.kry.util.exception.BadRequestException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest( {ServiceRegistryController.class, ServiceRegistryMapper.class, ObjectMapper.class})
public class ServiceRegistryControllerTest {

    private static final String SERVICE_INSTANCE_URL = "/service-registry/instance";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ServiceInstanceService serviceInstanceService;

    @Autowired
    private ObjectMapper objectMapper; // TO use default LocalDateTime serializer

    private ServiceInstance serviceInstanceFromDb;
    private ServiceInstanceDto serviceInstanceDto;

    @BeforeEach
    public void setUp(){
        serviceInstanceFromDb = new ServiceInstance();
        serviceInstanceFromDb.setId(1L);
        serviceInstanceFromDb.setName("TestServiceInstanceFromDB");
        serviceInstanceFromDb.setEndpoint("http://testurl.com/health");
        serviceInstanceFromDb.setDateCreated(LocalDateTime.of(2001,11,30,1,25));

        serviceInstanceDto = new ServiceInstanceDto();
        serviceInstanceDto.setName("TestServiceInstance");
        serviceInstanceDto.setEndpoint("http://testurl.com/health");
    }

    @Test
    public void contextLoads() {
        assertThat(mockMvc).isNotNull();
    }

    @Test
    public void testWhenNoDataAvailableThenReturnEmptyData() throws Exception {
        Mockito.when(serviceInstanceService.getAll()).thenReturn(new ArrayList<ServiceInstance>());
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get(SERVICE_INSTANCE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("[]");
    }

    @Test
    public void testWhenDataAvailableThenReturnSuccess() throws Exception {

        List<ServiceInstance> serviceInstancesFromDb = Arrays.asList(serviceInstanceFromDb);

        Mockito.when(serviceInstanceService.getAll()).thenReturn(serviceInstancesFromDb);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get(SERVICE_INSTANCE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceInstanceDto[] serviceInstancesResultDto = objectMapper
                .readValue(mvcResult.getResponse().getContentAsString(), ServiceInstanceDto[].class);
        assertThat(serviceInstancesResultDto).hasSize(1);
        assertThat(serviceInstancesResultDto[0]).isNotNull();

        ServiceInstanceDto serviceInstanceDtoResult = serviceInstancesResultDto[0];
        assertThat(serviceInstanceDtoResult.getId()).isEqualTo(1L);
        assertThat(serviceInstanceDtoResult.getName()).isEqualTo("TestServiceInstanceFromDB");
        assertThat(serviceInstanceDtoResult.getEndpoint()).isEqualTo("http://testurl.com/health");
        assertThat(serviceInstanceDtoResult.getDateCreated()).isEqualTo(LocalDateTime.of(2001,11,30,1,25));
        assertThat(serviceInstanceDtoResult.getServiceStatus()).isEqualTo(ServiceStatus.UNKNOWN);
    }

    @Test
    public void testWhenNewServiceInstanceAddedThenReturnSuccess() throws Exception {


        Mockito.when(serviceInstanceService.save(any(ServiceInstance.class)))
                .then(invocation -> {
                    ServiceInstance serviceInstance = invocation.getArgument(0);
                    assertThat(serviceInstance.getId()).isNull();
                    assertThat(serviceInstance.getName()).isEqualTo("TestServiceInstance");
                    assertThat(serviceInstance.getEndpoint()).isEqualTo("http://testurl.com/health");
                    return serviceInstanceFromDb;
                });

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post(SERVICE_INSTANCE_URL)
                .content(objectMapper.writeValueAsString(serviceInstanceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        ServiceInstanceDto serviceInstanceDtoResult = objectMapper
                .readValue(mvcResult.getResponse().getContentAsString(), ServiceInstanceDto.class);
        assertThat(serviceInstanceDtoResult).isNotNull();
        assertThat(serviceInstanceDtoResult.getId()).isEqualTo(1L);
        assertThat(serviceInstanceDtoResult.getName()).isEqualTo("TestServiceInstanceFromDB");
        assertThat(serviceInstanceDtoResult.getEndpoint()).isEqualTo("http://testurl.com/health");
        assertThat(serviceInstanceDtoResult.getServiceStatus()).isEqualTo(ServiceStatus.UNKNOWN);
    }

    @Test
    public void testWhenExistingServiceInstanceAddedThenReturnError() throws Exception {

        Mockito.when(serviceInstanceService.save(any(ServiceInstance.class)))
                .thenThrow(new BadRequestException("Service is already available in the system"));

        mockMvc.perform(MockMvcRequestBuilders
                .post(SERVICE_INSTANCE_URL)
                .content(objectMapper.writeValueAsString(serviceInstanceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.errorCode").value("ERR_1002"))
                .andExpect(jsonPath("$.message").value("Service is already available in the system"))
                .andReturn();
    }

    @Test
    public void testWhenServiceInstanceAddedWithInvalidNameThenReturnError() throws Exception {

        serviceInstanceDto.setName(null);
        mockMvc.perform(MockMvcRequestBuilders
                .post(SERVICE_INSTANCE_URL)
                .content(objectMapper.writeValueAsString(serviceInstanceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.errorCode").value("ERR_1001"))
                .andExpect(jsonPath("$.message").value("Missing required property: name"))
                .andReturn();
    }

    @Test
    public void testWhenServiceInstanceAddedWithInvalidEndpointThenReturnError() throws Exception {

        serviceInstanceDto.setName("TestServiceName");
        serviceInstanceDto.setEndpoint("hp://testurl.com/health");
        mockMvc.perform(MockMvcRequestBuilders
                .post(SERVICE_INSTANCE_URL)
                .content(objectMapper.writeValueAsString(serviceInstanceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.errorCode").value("ERR_1001"))
                .andExpect(jsonPath("$.message").value("Invalid url pattern"))
                .andReturn();
    }

    // TODO:: test status when there is a HelathRecord available
}
