package com.example.kry.dto;

import com.example.kry.util.enumeration.ServiceStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@Getter
@Setter
public class ServiceInstanceDto {

    private Long id;

    @NotEmpty(message = "Missing required property: name")
    @Pattern(regexp = "([a-zA-Z]+)", message = "Name should have only uppercase and lowercase characters without no spaces/special characters")
    private String name;

    @NotEmpty(message = "Missing required property: endpoint")
    @URL(message = "Invalid url pattern")
    private String endpoint;

    private LocalDateTime dateCreated;

    private ServiceStatus serviceStatus;
}
