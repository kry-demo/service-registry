package com.example.kry.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Builder
public class ErrorDto {

    private String errorCode;

    private String message;
}
