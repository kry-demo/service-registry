package com.example.kry.dto;

import com.example.kry.util.enumeration.ServiceStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ServiceInstanceHealthDto {

    private Long instanceId;

    private ServiceStatus serviceStatus;
}
