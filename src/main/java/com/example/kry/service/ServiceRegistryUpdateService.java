package com.example.kry.service;

import com.example.kry.dto.ServiceInstanceHealthDto;
import com.example.kry.model.HealthRecord;
import com.example.kry.model.ServiceInstance;
import com.example.kry.util.enumeration.ServerHealthStatus;
import com.example.kry.util.enumeration.ServiceStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ServiceRegistryUpdateService {

    @Autowired
    private HealthRecordService healthRecordService;

    @Autowired
    private ServerStatusPollerService serverStatusPollerService;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Async
    public void fetchAndUpdateServiceStatus(ServiceInstance serviceInstance){

        log.info("Instance Id:{}, Name: {}, Endpoint: {}", serviceInstance.getId(), serviceInstance.getName(), serviceInstance.getEndpoint());

        ServerHealthStatus serverHealthStatus = serverStatusPollerService.pollServerHealthStatus(serviceInstance.getEndpoint());
        log.debug("ServerHealthStatus: {}", serverHealthStatus);
        HealthRecord healthRecord = new HealthRecord();
        if (serverHealthStatus == ServerHealthStatus.SUCCESS) {
            healthRecord.setServiceStatus(ServiceStatus.OK);
        } else {
            healthRecord.setServiceStatus(ServiceStatus.FAIL);
        }
        log.debug("Checked Health: {}", healthRecord.getServiceStatus());

        healthRecord.setServiceInstance(serviceInstance);
        healthRecordService.save(healthRecord);
        simpMessagingTemplate.convertAndSend("/status/instance-health",
                ServiceInstanceHealthDto.builder().instanceId(healthRecord.getServiceInstance().getId())
                .serviceStatus(healthRecord.getServiceStatus())
                .build()
        );
        log.info("Health check complete. Instance Id:{}, Name: {}, Status: {}", serviceInstance.getId(),
                serviceInstance.getName(), healthRecord.getServiceStatus());
    }
}
