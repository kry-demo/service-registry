package com.example.kry.service;

import com.example.kry.model.ServiceInstance;
import com.example.kry.repository.ServiceInstanceRepository;
import com.example.kry.util.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ServiceInstanceService {

    @Autowired
    private ServiceInstanceRepository serviceInstanceRepository;

    public List<ServiceInstance> getAll() {
        return serviceInstanceRepository.findAll();
    }

    public ServiceInstance save(ServiceInstance serviceInstance) throws BadRequestException {

        if(serviceInstanceRepository.findByName(serviceInstance.getName()).isPresent()){
            log.error("Service is already available in DB. Service name: {}", serviceInstance.getName());
            throw new BadRequestException("Service is already available in the system");
        }

        serviceInstance.setDateCreated(LocalDateTime.now());
        return serviceInstanceRepository.save(serviceInstance);
    }
}
