package com.example.kry.service;

import com.example.kry.model.ServiceInstance;
import com.example.kry.repository.ServiceInstanceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class HealthCheckService {

    @Autowired
    private ServiceInstanceRepository serviceInstanceRepository;

    @Autowired
    private ServiceRegistryUpdateService serviceRegistryUpdateService;

    @Scheduled(fixedRate = 15000)
    public void runHealthCheck() {
        log.info("Health check polling triggered at {}", LocalDateTime.now());

        List<ServiceInstance> serviceInstances = loadServiceInstances();

        for (int i = 0; i < serviceInstances.size(); i++) {
            ServiceInstance serviceInstance = serviceInstances.get(i);
            try{
                serviceRegistryUpdateService.fetchAndUpdateServiceStatus(serviceInstance);
            }
            catch (Exception exception){
                log.warn("Error occurred when executing async task: {}", exception.getLocalizedMessage());
            }
        }
    }

    private List<ServiceInstance> loadServiceInstances() {
        return serviceInstanceRepository.findAll();
    }
}