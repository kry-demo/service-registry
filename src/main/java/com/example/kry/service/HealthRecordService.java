package com.example.kry.service;

import com.example.kry.model.HealthRecord;
import com.example.kry.repository.HealthRecordRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Slf4j
public class HealthRecordService {

    @Autowired
    HealthRecordRepository healthRecordRepository;

    public HealthRecord save(HealthRecord healthRecord){

        healthRecord.setDateCreated(LocalDateTime.now());
        return healthRecordRepository.save(healthRecord);
    }
}
