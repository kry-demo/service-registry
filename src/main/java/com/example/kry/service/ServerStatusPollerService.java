package com.example.kry.service;

import com.example.kry.util.enumeration.ServerHealthStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class ServerStatusPollerService {

    @Autowired
    private RestTemplate restTemplate;

    public ServerHealthStatus pollServerHealthStatus(String endpoint){

        ServerHealthStatus serverHealthStatus = ServerHealthStatus.FAILED;
        try {
            ResponseEntity<String> response
                    = restTemplate.getForEntity(endpoint, String.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                serverHealthStatus = ServerHealthStatus.SUCCESS;
            }
        }catch (Exception exception){
            log.debug("Exception thrown when accessing url:" + exception);
        }
        return serverHealthStatus;
    }
}
