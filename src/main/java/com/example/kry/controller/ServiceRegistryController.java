package com.example.kry.controller;

import com.example.kry.dto.ServiceInstanceDto;
import com.example.kry.service.ServiceInstanceService;
import com.example.kry.mappers.ServiceRegistryMapper;
import com.example.kry.util.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/service-registry/instance")
@CrossOrigin(origins = "*")
public class ServiceRegistryController {

    @Autowired
    private ServiceInstanceService serviceInstanceService;

    @Autowired
    private ServiceRegistryMapper serviceRegistryMapper;

    @GetMapping
    public List<ServiceInstanceDto> getAll() {
        return serviceRegistryMapper
                .mapToServiceInstanceDto(serviceInstanceService.getAll());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ServiceInstanceDto save(@Valid @RequestBody ServiceInstanceDto serviceInstanceDto)
            throws BadRequestException {

        return serviceRegistryMapper.mapToServiceInstanceDto(
                serviceInstanceService.save(
                        serviceRegistryMapper.mapToServiceInstance(serviceInstanceDto)
                ));
    }
}
