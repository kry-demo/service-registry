package com.example.kry.mappers;

import com.example.kry.dto.ServiceInstanceDto;
import com.example.kry.model.ServiceInstance;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ServiceRegistryMapper {

    @Mapping(source = "serviceInstance.latestHealthRecord.serviceStatus", target = "serviceStatus", defaultValue = "UNKNOWN")
    ServiceInstanceDto mapToServiceInstanceDto(ServiceInstance serviceInstance);

    ServiceInstance mapToServiceInstance(ServiceInstanceDto serviceInstanceDto);

    List<ServiceInstanceDto> mapToServiceInstanceDto(List<ServiceInstance> serviceInstances);

}
