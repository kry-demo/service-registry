package com.example.kry.util.enumeration;

public enum ServiceStatus {
    UNKNOWN,
    OK,
    FAIL
}
