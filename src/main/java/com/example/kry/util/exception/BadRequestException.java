package com.example.kry.util.exception;

public class BadRequestException extends Exception{

    public BadRequestException(String message){
        super(message);
    }
}
