package com.example.kry.util.exception.handler;

import com.example.kry.dto.ErrorDto;
import com.example.kry.util.exception.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleInputValidationException(MethodArgumentNotValidException exception){

        return ErrorDto.builder()
                .errorCode("ERR_1001") // TODO: Need to define the Error code based on JSR validation
                .message(exception.getBindingResult().getFieldErrors().get(0).getDefaultMessage())
                .build();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleInputValidationException(BadRequestException exception){

        return ErrorDto.builder()
                .errorCode("ERR_1002")
                .message(exception.getMessage())
                .build();
    }
}
