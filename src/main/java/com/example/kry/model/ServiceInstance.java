package com.example.kry.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class ServiceInstance {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private String name;

    private String endpoint;

    private LocalDateTime dateCreated;

    @ManyToOne
    @JoinFormula("(SELECT hr.id FROM health_record hr WHERE hr.service_instance_id = id ORDER BY hr.date_created DESC LIMIT 1)")
    private HealthRecord latestHealthRecord;
}
