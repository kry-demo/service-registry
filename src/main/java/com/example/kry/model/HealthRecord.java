package com.example.kry.model;

import com.example.kry.util.enumeration.ServiceStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class HealthRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private ServiceStatus serviceStatus;

    private LocalDateTime dateCreated;

    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceInstance serviceInstance;
}
