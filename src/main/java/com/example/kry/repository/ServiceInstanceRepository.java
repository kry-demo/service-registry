package com.example.kry.repository;

import com.example.kry.model.ServiceInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ServiceInstanceRepository extends JpaRepository<ServiceInstance, Long > {

    Optional<ServiceInstance> findByName(String name);
}
